HandsWidth
===========
__Measure app for Apple Vision Pro__

_Hands is ruler._

<img src="HandsWidth/Supporting files/README assets/icon.png" width="64">

<a href="https://apps.apple.com/app/id6475769879" target="blank">
    <img src="HandsWidth/Supporting files/README assets/appstore_badge.svg">
</a>

<img src="HandsWidth/Supporting files/README assets/screenshot1200w.png" width="600">


Description
------------
Measure the distance between both hands intuitively.

App shows the measurement of the distance between the tips of the index fingers, from the left hand to the right hand.

Unit option
- centiMeters
- meters
- inches
- feet
- yards

Sub function
- Fix a pointer by indirect tap.


App Store link
---------------
[apps.apple.com/app/id6475769879](https://apps.apple.com/app/id6475769879)


Source code link
-----------------
[github.com/FlipByBlink/HandsWidth](https://github.com/FlipByBlink/HandsWidth)

### Source code (Mirror) link
[gitlab.com/FlipByBlink/HandsWidth_Mirror](https://gitlab.com/FlipByBlink/HandsWidth_Mirror)


Contact
--------
sear_pandora_0x@icloud.com


* * *

<br>
<br>
<br>
<br>


Privacy Policy for App Store
----------------------------
2024-02-02

### English
This application don't collect user infomation.

### Japanese
このアプリ自身において、ユーザーの情報を一切収集しません。


<br>
<br>
<br>
<br>


<!-- URL "Support page for App Store" -->
<!-- https://flipbyblink.github.io/HandsWidth/ -->
<!-- URL "Privacy Policy for App Store" -->
<!-- https://flipbyblink.github.io/HandsWidth/#privacy-policy-for-app-store -->
